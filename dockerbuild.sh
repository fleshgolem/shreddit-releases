cd api
docker build --pull -t shreddit-releases-api .
docker tag shreddit-releases-api fleshgolem/shreddit-releases-api:latest
docker push fleshgolem/shreddit-releases-api:latest
cd ../web
docker build --pull -t shreddit-releases-web .
docker tag shreddit-releases-web fleshgolem/shreddit-releases-web:latest
docker push fleshgolem/shreddit-releases-web:latest