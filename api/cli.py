import asyncio

import typer

from config import Config
from database.shared import SessionLocal
from database.UserDB import UserDB
from database.LinkDB import LinkDB

app = typer.Typer()
config = Config()


@app.command()
def create_user(username: str, password: str):
    with SessionLocal() as db:
        user = UserDB()
        user.username = username
        user.password = password
        db.add(user)
        db.commit()


@app.command()
def placeholder():
    pass


@app.command()
def spotify_sync():
    from spotify_lister import sync

    async def _sync():
        with SessionLocal() as db:
            await sync(db)

    asyncio.run(_sync())


@app.command()
def spotify_clear():
    from spotify_lister import clear_list

    async def _sync():
        with SessionLocal() as db:
            await clear_list(db)

    asyncio.run(_sync())


if __name__ == "__main__":
    app()
