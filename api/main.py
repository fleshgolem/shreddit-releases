import datetime
from base64 import b64decode
from contextlib import asynccontextmanager
from typing import Optional

import uvicorn  # type: ignore
from fastapi import Depends, FastAPI, HTTPException, Request, Response, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from fastapi.staticfiles import StaticFiles
from fastapi_cache import FastAPICache  # type: ignore
from fastapi_cache.backends.redis import RedisBackend  # type: ignore
from fastapi_cache.decorator import cache  # type: ignore
from redis.asyncio import Redis
from sqlalchemy import select
from sqlalchemy.orm import Session, joinedload
from starlette.responses import RedirectResponse

import spotify  # type: ignore
from config import Config
from cover_art import add_cover_to_release, find_cover_art
from database.LabelDB import LabelDB
from database.LinkDB import LinkDB
from database.ReleaseDB import ReleaseDB
from database.shared import SessionLocal
from database.SpotifyUserDB import SpotifyUserDB
from database.TagDB import TagDB
from database.UserDB import AuthTokenDB, Role, UserDB
from model.Release import Release, ReleaseRequest, ReleaseResponse
from model.User import (
    LoginRequest,
    LoginResponse,
    User,
    UserEditRequest,
    UserList,
    UserRequest,
)
from utils import sanitized_filename

config = Config()


@asynccontextmanager
async def lifespan(app: FastAPI):
    redis = Redis.from_url(
        config.CACHE_REDIS_URL, encoding="utf8", decode_responses=True
    )
    FastAPICache.init(RedisBackend(redis), prefix="shredditreleases-cache")
    yield


app = FastAPI(lifespan=lifespan)

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://127.0.0.1:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.add_middleware(GZipMiddleware)

app.mount("/api/covers", StaticFiles(directory=config.COVERS_DIR), name="covers")


token_scheme = HTTPBearer()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_current_user(
    token: HTTPAuthorizationCredentials = Depends(token_scheme), db=Depends(get_db)
) -> UserDB:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    auth_token = AuthTokenDB.find(token.credentials, db)
    if not auth_token:
        raise credentials_exception
    return auth_token.user


async def get_current_admin(current_user: UserDB = Depends(get_current_user)) -> UserDB:
    if current_user.role != Role.admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    return current_user


# default keybuilder is kinda borked because it encodes dependency params as well.
# Fortunately we can just use a very simple one
def simple_key_builder(
    func,
    namespace: Optional[str] = "",
    request: Request | None = None,
    response: Response | None = None,
    *args,
    **kwargs,
):
    prefix = FastAPICache.get_prefix()
    cache_key = f"{prefix}:{namespace}:{func.__module__}:{func.__name__}"
    return cache_key


@app.get("/api/releases", response_model=ReleaseResponse)
@cache(expire=600, namespace="releases", key_builder=simple_key_builder)
async def all_releases(db: Session = Depends(get_db)):
    one_year_ago = datetime.date.today() - datetime.timedelta(days=365)
    stmt = (
        select(ReleaseDB)
        .filter(ReleaseDB.release_date >= one_year_ago)
        .options(joinedload(ReleaseDB.label))
        .options(joinedload(ReleaseDB.tags))
        .order_by(ReleaseDB.release_date)
    )
    releases = db.execute(stmt).unique().scalars().all()
    return ReleaseResponse(releases=[Release.model_validate(r) for r in releases])


@app.post("/api/login", response_model=LoginResponse)
def login(request: LoginRequest, db: Session = Depends(get_db)):
    stmt = select(UserDB).filter_by(username=request.username)
    user = db.execute(stmt).scalars().first()
    if not user or not user.verify_password(request.password):
        raise HTTPException(status.HTTP_403_FORBIDDEN, "Login Invalid")
    token = user.create_auth_token(db)
    return LoginResponse.from_token(token)


@app.get("/api/auth", response_model=User)
def check_auth(user: UserDB = Depends(get_current_user), db: Session = Depends(get_db)):
    return user


@app.post("/api/logout")
def logout(
    user: UserDB = Depends(get_current_user),
    token: HTTPAuthorizationCredentials = Depends(token_scheme),
    db: Session = Depends(get_db),
):
    auth_token = AuthTokenDB.find(token.credentials, db)
    db.delete(auth_token)
    db.commit()
    return ""


@app.put("/api/user", response_model=LoginResponse)
def edit_profile(
    request: UserEditRequest,
    user: UserDB = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    if not user.verify_password(request.old_password):
        raise HTTPException(status.HTTP_403_FORBIDDEN, "Login Invalid")

    if request.username:
        user.username = request.username
    if request.new_password:
        user.password = request.new_password
        user.invalidate_login(db)
    db.add(user)
    token = user.create_auth_token(db)
    return LoginResponse.from_token(token)


@app.post("/api/releases", response_model=Release)
async def add_release(
    request: ReleaseRequest,
    user: UserDB = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    release = ReleaseDB()
    release.submitter_id = user.id
    add_data_to_release(release, request, db)
    await FastAPICache.clear(namespace="releases")
    return release


@app.put("/api/releases/{release_id}", response_model=Release)
async def edit_release(
    release_id: int,
    request: ReleaseRequest,
    user: UserDB = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    stmt = select(ReleaseDB).filter_by(id=release_id)
    release = db.execute(stmt).scalars().first()
    if not release:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Release not found")
    add_data_to_release(release, request, db)
    await FastAPICache.clear(namespace="releases")
    return release


@app.delete("/api/releases/{release_id}")
async def delete_release(
    release_id: int,
    user: UserDB = Depends(get_current_user),
    db: Session = Depends(get_db),
):
    stmt = select(ReleaseDB).filter_by(id=release_id)
    release = db.execute(stmt).scalars().first()
    if not release:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Release not found")
    db.delete(release)
    db.commit()
    await FastAPICache.clear(namespace="releases")
    return ""


def add_data_to_release(release: ReleaseDB, request: ReleaseRequest, db: Session):
    release.artist = request.artist.replace("\u200c", "")
    release.title = request.title.replace("\u200c", "")
    release.release_type = request.release_type
    release.release_date = request.release_date
    release.label = LabelDB.get_or_create(request.label, db)
    release.description = request.description
    for link in release.links:
        db.delete(link)

    for reql in request.links:
        link = LinkDB()
        link.url = reql
        link.release = release
        db.add(link)

    release.tags = []
    for t in request.tags:
        tag = TagDB.get_or_create(t, db)
        release.tags.append(tag)

    db.add(release)
    db.commit()
    if request.cover_data and request.cover_extension:
        data = b64decode(request.cover_data)
        add_cover_to_release(
            release=release,
            filename=sanitized_filename(
                f"{request.artist}-{request.title}.{request.cover_extension}"
            ),
            file_data=data,
            cover_dir=config.COVERS_DIR,
        )

    if not release.cover_url:
        find_cover_art(release, db, config.COVERS_DIR)
    db.commit()
    db.refresh(release)


@app.get("/api/users", response_model=UserList)
def list_users(
    current_user: UserDB = Depends(get_current_admin), db: Session = Depends(get_db)
):
    stmt = select(UserDB).order_by(UserDB.username)
    users = db.execute(stmt).scalars().all()
    return UserList(users=[User.model_validate(u) for u in users])


@app.post("/api/users", response_model=User)
def create_user(
    request: UserRequest,
    current_user: UserDB = Depends(get_current_admin),
    db: Session = Depends(get_db),
):
    if not request.password or not request.username:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Missing password")
    user = UserDB()
    user.username = request.username
    user.password = request.password
    user.role = request.role
    db.add(user)
    db.commit()
    return User.model_validate(user)


@app.put("/api/users/{user_id}", response_model=User)
def edit_user(
    user_id: str,
    request: UserRequest,
    current_user: UserDB = Depends(get_current_admin),
    db: Session = Depends(get_db),
):
    stmt = select(UserDB).filter_by(id=user_id)
    user = db.execute(stmt).scalars().first()
    if not user:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    if request.username:
        user.username = request.username
    if request.password:
        user.password = request.password
    user.role = request.role
    db.add(user)
    db.commit()
    return User.model_validate(user)


@app.delete("/api/users/{user_id}")
def delete_user(
    user_id: str,
    current_user: UserDB = Depends(get_current_admin),
    db: Session = Depends(get_db),
):
    stmt = select(UserDB).filter_by(id=user_id)
    user = db.execute(stmt).scalars().first()
    if not user:
        raise HTTPException(status.HTTP_404_NOT_FOUND)
    db.delete(user)
    db.commit()
    return ""


@app.get("/api/spotify/auth")
def start_spotify_auth():
    if not config.ENABLE_SPOTIFY_AUTH:
        raise HTTPException(status_code=404)
    scopes = (
        "user-top-read",
        "playlist-read-private",
        "playlist-modify-private",
        "playlist-modify-public",
        "playlist-read-collaborative",
        "user-library-modify",
        "user-library-read",
        "user-read-private",
    )
    oauth = spotify.OAuth2(
        config.SPOTIFY_CLIENT_ID, config.SPOTIFY_CALLBACK_URL or "", scopes=scopes
    )
    return RedirectResponse(oauth.url)


@app.get("/api/spotify/callback")
async def spotify_callback(code: str, db: Session = Depends(get_db)) -> str:
    client = spotify.Client(
        config.SPOTIFY_CLIENT_ID or "", config.SPOTIFY_CLIENT_SECRET or ""
    )
    await SpotifyUserDB.update(client, code, config.SPOTIFY_CALLBACK_URL or "", db)
    return "SUCCESS"


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True, reload_dirs=".")
