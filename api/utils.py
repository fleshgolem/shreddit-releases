import re

_filename_re = re.compile(r"[/\\?%*:|\"<>\x7F\x00-\x1F]")


def sanitized_filename(input: str) -> str:
    return _filename_re.sub("-", input)
