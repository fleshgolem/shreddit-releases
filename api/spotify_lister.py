from asyncio import gather
from datetime import datetime, timedelta

from sqlalchemy import select, delete
from sqlalchemy.orm import Session

from config import Config
from database.ReleaseDB import ReleaseDB
from database.SpotifyPlaylistEntryDB import SpotifyPlaylistEntryDB
from database.SpotifyUserDB import SpotifyUserDB
from spotify import Album, Client, Playlist, Track, User
from fuzzywuzzy import fuzz  # type: ignore
import unidecode

config = Config()


def get_client() -> Client:
    return Client(config.SPOTIFY_CLIENT_ID or "", config.SPOTIFY_CLIENT_SECRET or "")


async def get_user(client: Client, session: Session) -> User:
    db_user = SpotifyUserDB.get(session)
    asdf = User.from_token(client, None, db_user.refresh_token)
    return await asdf


async def get_playlist(client: Client) -> Playlist:
    return await client.get_playlist(config.SPOTIFY_PLAYLIST_ID or "")


def normalized(input: str | None) -> str:
    return unidecode.unidecode(input or "").lower()


def album_matches_release(album: Album, release: ReleaseDB):
    ratio_title = fuzz.ratio(normalized(album.name), normalized(release.title))
    ratio_artist = fuzz.ratio(normalized(album.artist.name), normalized(release.artist))
    if ratio_artist < 90 or ratio_title < 90:
        print(
            f"Album {album.artist.name} - {album.name} did not match release {release.artist} - {release.title}, skipping"
        )
        return False
    return True


async def find_album(release: ReleaseDB, client: Client) -> Album | None:
    q = f"{release.artist} {release.title}"
    result = await client.search(
        q, types=["album"], limit=5, should_include_external=False, market="DE"
    )

    for r in result.albums:
        if r.type == "album" and album_matches_release(r, release):
            return r
    return None


async def sync(session: Session):
    client = get_client()
    user = await get_user(client, session)
    playlist = await get_playlist(client)
    today = datetime.utcnow()
    three_months_ago = today - timedelta(days=92)

    release_stmt = (
        select(ReleaseDB)
        .filter(ReleaseDB.release_date >= three_months_ago.date())
        .filter(ReleaseDB.release_date <= today.date())
        .order_by(ReleaseDB.release_date)
    )
    releases = session.execute(release_stmt).unique().scalars().all()

    for r in releases:
        if SpotifyPlaylistEntryDB.get_for_album(r.id, session):
            print(f"Album {r.artist} - {r.title} already listed, skipping")
            continue
        album = await find_album(r, client)
        if album:
            await add_album_to_playlist(r, album, playlist, user, client, session)
    await prune_list(playlist, user, today)
    await client.close()


async def prune_list(playlist: Playlist, user: User, now: datetime):
    tracks = await playlist.get_all_tracks()
    three_months_ago = now - timedelta(days=92)

    old_tracks = [t for t in tracks if t.added_at < three_months_ago]
    track_uris = [t.uri for t in old_tracks]
    await user.remove_tracks(playlist, *track_uris)
    print(f"Pruned playlist: {track_uris}")


async def clear_list(session: Session):
    client = get_client()
    print(client)
    user = await get_user(client, session)
    print(user)
    playlist = await get_playlist(client)
    print(playlist)
    tracks = await playlist.get_all_tracks()
    print(tracks)
    track_uris = [t.uri for t in tracks]
    await user.remove_tracks(playlist, *track_uris)
    session.execute(delete(SpotifyPlaylistEntryDB))
    session.commit()


async def add_album_to_playlist(
    release: ReleaseDB,
    album: Album,
    playlist: Playlist,
    user: User,
    client: Client,
    session: Session,
):
    tracks = await get_album_tracks(album, client)

    tracks = sorted(tracks, key=lambda x: x.popularity, reverse=True)
    extended_tracks = [t for t in tracks if t.duration > 2.5 * 60 * 1000]
    if len(extended_tracks) > 0:
        tracks = extended_tracks
    not_too_extended_tracks = [t for t in tracks if t.duration < 8 * 60 * 1000]
    if len(not_too_extended_tracks) > 0:
        tracks = not_too_extended_tracks
    snapshot_id = await user.add_tracks(playlist, 0, tracks[0])
    entry = SpotifyPlaylistEntryDB()
    entry.artist = album.artist
    entry.track = tracks[0].name
    entry.album = album.name
    entry.snapshot_id = snapshot_id
    entry.album_id = f"{release.id}"
    session.add(entry)
    session.commit()
    print(
        f"Added {release.artist} - {release.title} | {album.artist.name} - {album.name}"
    )


async def get_album_tracks(album: Album, client: Client) -> list[Track]:
    tracks = await album.get_tracks()
    tasks = [client.get_track(t.id) for t in tracks]
    return await gather(*tasks)
