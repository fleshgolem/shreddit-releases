from datetime import date
from typing import List, Optional

from pydantic import ConfigDict, BaseModel


class Label(BaseModel):
    name: str
    model_config = ConfigDict(from_attributes=True)


class Tag(BaseModel):
    id: int
    name: str
    order: int
    foreground_color: Optional[str] = None
    background_color: Optional[str] = None
    model_config = ConfigDict(from_attributes=True)


class Link(BaseModel):
    url: str
    model_config = ConfigDict(from_attributes=True)


class Release(BaseModel):
    id: int
    artist: str
    title: str
    release_type: str
    release_date: date
    description: str
    label: Optional[Label] = None
    tags: List[Tag]
    links: List[Link]
    cover_url: Optional[str] = None
    cover_thumbnail_url: Optional[str] = None
    date_added: date
    model_config = ConfigDict(from_attributes=True)


class ReleaseResponse(BaseModel):
    releases: List[Release]


class ReleaseRequest(BaseModel):
    artist: str
    title: str
    release_type: str
    release_date: date
    description: str
    label: str
    tags: List[str]
    links: List[str]
    cover_data: Optional[str] = None
    cover_extension: Optional[str] = None
