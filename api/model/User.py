from typing import Optional

from pydantic import ConfigDict, BaseModel

from database.UserDB import AuthTokenDB, Role


class User(BaseModel):
    id: str
    username: str
    role: Role
    model_config = ConfigDict(from_attributes=True)


class LoginResponse(BaseModel):
    user: User
    token: str

    @staticmethod
    def from_token(token: AuthTokenDB) -> "LoginResponse":
        return LoginResponse(token=token.id, user=User.model_validate(token.user))


class LoginRequest(BaseModel):
    username: str
    password: str


class UserEditRequest(BaseModel):
    username: Optional[str] = None
    old_password: str
    new_password: Optional[str] = None


class UserList(BaseModel):
    users: list[User]


class UserRequest(BaseModel):
    username: str
    role: Role
    password: Optional[str] = None
