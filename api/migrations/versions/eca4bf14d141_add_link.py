"""add link

Revision ID: eca4bf14d141
Revises: e5f3d71a3457
Create Date: 2021-08-08 21:42:59.997671

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "eca4bf14d141"
down_revision = "e5f3d71a3457"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "links",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("url", sa.Text(), nullable=True),
        sa.Column("release_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["release_id"],
            ["releases.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_links_id"), "links", ["id"], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f("ix_links_id"), table_name="links")
    op.drop_table("links")
    # ### end Alembic commands ###
