from sqlalchemy import Integer, Text, select
from sqlalchemy.orm import Mapped, mapped_column, Session

from database.shared import Base


class SpotifyPlaylistEntryDB(Base):
    __tablename__ = "spotify_playlist_entry"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    artist: Mapped[str] = mapped_column(Text, nullable=False)
    album: Mapped[str] = mapped_column(Text, nullable=False)
    track: Mapped[str] = mapped_column(Text, nullable=False)
    snapshot_id: Mapped[str] = mapped_column(Text, nullable=False)
    album_id: Mapped[str] = mapped_column(Text, nullable=False, index=True)

    @staticmethod
    def get_for_album(album_id: int, session: Session):
        return (
            session.execute(
                select(SpotifyPlaylistEntryDB).filter(
                    SpotifyPlaylistEntryDB.album_id == album_id
                )
            )
            .scalars()
            .first()
        )
