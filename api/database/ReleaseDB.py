from datetime import date

from sqlalchemy import Column, Date, ForeignKey, Integer, String, Table, Text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .LabelDB import LabelDB
from .shared import Base
from .TagDB import TagDB

tags_releases = Table(
    "tags_releases",
    Base.metadata,
    Column("tag_id", ForeignKey("tags.id")),
    Column("release_id", ForeignKey("releases.id")),
)


class ReleaseDB(Base):
    __tablename__ = "releases"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    artist: Mapped[str | None] = mapped_column(Text)
    title: Mapped[str | None] = mapped_column(Text)
    release_type: Mapped[str | None] = mapped_column(String(100), index=True)
    release_date: Mapped[date | None] = mapped_column(Date)
    description: Mapped[str | None] = mapped_column(Text)
    date_added: Mapped[date] = mapped_column(
        Date, nullable=False, server_default="2021-08-13"
    )

    label_id: Mapped[int | None] = mapped_column(
        Integer, ForeignKey("labels.id"), nullable=True
    )
    label: Mapped[LabelDB] = relationship("LabelDB", foreign_keys=[label_id])

    tags: Mapped[list[TagDB]] = relationship("TagDB", secondary=tags_releases)

    cover_url: Mapped[str | None] = mapped_column(Text)
    cover_thumbnail_url: Mapped[str | None] = mapped_column(Text)

    submitter_id: Mapped[str | None] = mapped_column(
        String(32), ForeignKey("users.id"), nullable=True
    )
    links = relationship(
        "LinkDB", back_populates="release", lazy="joined", cascade="all, delete"
    )

    def __init__(self):
        self.date_added = date.today()

    def __repr__(self):
        return f"<ReleaseDB> {self.artist} - {self.title} - {self.release_date}"
