from sqlalchemy import Integer, String, func, select
from sqlalchemy.orm import Mapped, Session, mapped_column

from .shared import Base


class TagDB(Base):
    __tablename__ = "tags"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    name: Mapped[str | None] = mapped_column(String(150), index=True)
    order: Mapped[int] = mapped_column(Integer, index=True, nullable=False)
    foreground_color: Mapped[str | None] = mapped_column(String(7), nullable=True)
    background_color: Mapped[str | None] = mapped_column(String(7), nullable=True)

    @staticmethod
    def get_or_create(name: str, db: Session) -> "TagDB":
        tag = db.execute(select(TagDB).filter(TagDB.name == name)).scalars().first()
        if not tag:
            tag = TagDB()
            tag.order = db.scalar(select(func.count(TagDB.id))) or 0
            tag.name = name
        return tag
