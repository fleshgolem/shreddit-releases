from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey, Integer, Text
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .shared import Base

if TYPE_CHECKING:
    from .ReleaseDB import ReleaseDB


class LinkDB(Base):
    __tablename__ = "links"

    id: Mapped[int] = mapped_column("id", Integer, primary_key=True, index=True)
    url: Mapped[str | None] = mapped_column(Text)

    release_id: Mapped[int] = mapped_column(
        Integer, ForeignKey("releases.id"), nullable=False
    )
    release: Mapped["ReleaseDB"] = relationship(
        "ReleaseDB",
        foreign_keys=[release_id],
    )
