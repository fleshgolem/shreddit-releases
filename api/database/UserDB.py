import enum
from typing import Optional

from passlib.context import CryptContext
from sqlalchemy import Enum, ForeignKey, String, select
from sqlalchemy.orm import Mapped, Session, mapped_column, relationship

from .shared import Base, generate_uuid

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Role(enum.Enum):
    admin = "admin"
    contributor = "contributor"


class AuthTokenDB(Base):
    __tablename__ = "auth_tokens"
    id: Mapped[str] = mapped_column(
        String(32), primary_key=True, index=True, default=generate_uuid
    )
    user_id: Mapped[str] = mapped_column(
        String(32), ForeignKey("users.id"), nullable=False
    )
    user: Mapped["UserDB"] = relationship(
        "UserDB", foreign_keys=[user_id], lazy="joined"
    )

    def __init__(self, user_id: str):
        self.user_id = user_id

    @staticmethod
    def find(id: str, session: Session) -> Optional["AuthTokenDB"]:
        return session.execute(select(AuthTokenDB).filter_by(id=id)).scalars().first()


class UserDB(Base):
    __tablename__ = "users"

    id: Mapped[str] = mapped_column(
        String(32), primary_key=True, index=True, default=generate_uuid
    )
    username: Mapped[str | None] = mapped_column(String(150), unique=True)
    hashed_password: Mapped[str | None] = mapped_column(String(100))
    role: Mapped[Role] = mapped_column(
        Enum(Role), nullable=False, server_default=Role.contributor.value
    )
    auth_tokens: Mapped[list[AuthTokenDB]] = relationship(
        "AuthTokenDB", back_populates="user", cascade="all,delete"
    )

    @property
    def password(self) -> str:
        return ""

    @password.setter
    def password(self, value: str):
        self.hashed_password = pwd_context.hash(value)

    def verify_password(self, password: str) -> bool:
        return pwd_context.verify(password, self.hashed_password)

    def invalidate_login(self, db: Session):
        for token in self.auth_tokens:
            db.delete(token)

    def create_auth_token(self, db: Session) -> AuthTokenDB:
        token = AuthTokenDB(self.id)
        db.add(token)
        db.commit()
        return token
