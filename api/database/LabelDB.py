from sqlalchemy import Integer, String, select
from sqlalchemy.orm import Mapped, Session, mapped_column

from .shared import Base


class LabelDB(Base):
    __tablename__ = "labels"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    name: Mapped[str | None] = mapped_column(String(150), index=True)

    @staticmethod
    def get_or_create(name: str, db: Session) -> "LabelDB":
        label = (
            db.execute(select(LabelDB).filter(LabelDB.name == name)).scalars().first()
        )
        if not label:
            label = LabelDB()
            label.name = name
        return label
