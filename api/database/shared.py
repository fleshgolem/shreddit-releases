from uuid import uuid4

from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase, sessionmaker

from config import Config

# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"
config = Config()
engine = create_engine(config.SQLALCHEMY_URL, pool_recycle=280)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine, future=True)


class Base(DeclarativeBase):
    pass


def generate_uuid() -> str:
    return uuid4().hex
