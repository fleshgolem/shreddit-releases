from base64 import b64encode

from sqlalchemy import Integer, Text, select
from sqlalchemy.orm import Mapped, Session, mapped_column

from database.shared import Base


class SpotifyUserDB(Base):
    __tablename__ = "spotify_user"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)
    access_token: Mapped[str] = mapped_column(Text, nullable=False)
    refresh_token: Mapped[str] = mapped_column(Text, nullable=False)
    spotify_id: Mapped[str] = mapped_column(Text, nullable=False)
    display_name: Mapped[str] = mapped_column(Text, nullable=False)

    @staticmethod
    def get(session: Session) -> "SpotifyUserDB":
        user = session.execute(select(SpotifyUserDB)).scalars().first()
        if not user:
            user = SpotifyUserDB()
        return user

    @staticmethod
    async def update(client, code: str, redirect_uri: str, session: Session):
        import spotify

        route = ("POST", "https://accounts.spotify.com/api/token")
        payload = {
            "redirect_uri": redirect_uri,
            "grant_type": "authorization_code",
            "code": code,
        }

        client_id = client.http.client_id
        client_secret = client.http.client_secret

        auth = b64encode(":".join((client_id, client_secret)).encode()).decode()
        headers = {
            "Authorization": f"Basic {auth}",
            "Content-Type": "application/x-www-form-urlencoded",
        }

        raw = await client.http.request(route, headers=headers, params=payload)

        token = raw["access_token"]
        refresh_token = raw["refresh_token"]

        suser = await spotify.User.from_token(client, token, refresh_token)
        # This is stupid, but it works
        user = SpotifyUserDB.get(session)
        user.access_token = token
        user.refresh_token = refresh_token
        user.spotify_id = suser.id
        user.display_name = suser.display_name

        session.add(user)
        session.commit()
