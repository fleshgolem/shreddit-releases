from os import path
from typing import Optional

import requests
from bs4 import BeautifulSoup
from bs4.element import Tag
from PIL import Image
from sqlalchemy.orm import Session

from database.LinkDB import LinkDB
from database.ReleaseDB import ReleaseDB


def find_cover_art(release: ReleaseDB, db: Session, target_dir: str):
    for link in release.links:
        find_bandcamp_cover(link, release, db, target_dir)


def find_bandcamp_cover(link: LinkDB, release: ReleaseDB, db: Session, target_dir: str):
    if link.url:
        r = requests.get(link.url)
        html = r.text
        img_url = parse_bandcamp_html(html)
        if img_url:
            r = requests.get(img_url)
            bytes = r.content

            filename = img_url.rsplit("/", 1)[-1]
            add_cover_to_release(release, filename, bytes, target_dir)
            db.add(release)
            db.commit()


def add_cover_to_release(release, filename, file_data, cover_dir):
    thumbnail_filename = f"t_{filename}"
    file_out = path.join(cover_dir, filename)
    t_file_out = path.join(cover_dir, thumbnail_filename)
    with open(file_out, "wb") as f:
        f.write(file_data)
    with Image.open(file_out) as img:
        img.thumbnail((200, 200))
        img.save(t_file_out)
    img_path = f"/api/covers/{filename}"
    t_img_path = f"/api/covers/{thumbnail_filename}"
    release.cover_url = img_path
    release.cover_thumbnail_url = t_img_path


def parse_bandcamp_html(text: str) -> Optional[str]:
    soup = BeautifulSoup(text, "html.parser")
    art = soup.find("div", id="tralbumArt")
    if art and isinstance(art, Tag):
        fullsize = art.find("a", class_="popupImage")
        if fullsize and isinstance(fullsize, Tag):
            img_url = fullsize["href"]
            return img_url  # type: ignore
    return None
