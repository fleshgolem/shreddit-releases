from pydantic_settings import BaseSettings, SettingsConfigDict


class Config(BaseSettings):
    SQLALCHEMY_URL: str = ""
    COVERS_DIR: str = ""
    CACHE_REDIS_URL: str = ""
    ENABLE_SPOTIFY_AUTH: bool = False
    SPOTIFY_CALLBACK_URL: str | None
    SPOTIFY_CLIENT_ID: str | None
    SPOTIFY_CLIENT_SECRET: str | None
    SPOTIFY_PLAYLIST_ID: str | None
    model_config = SettingsConfigDict(env_file=".env")
