from csv import reader
from typing import List

from dateutil.parser import parse
from sqlalchemy import delete
from sqlalchemy.orm import Session

from config import Config
from cover_art import find_cover_art
from database.LabelDB import LabelDB
from database.LinkDB import LinkDB
from database.ReleaseDB import ReleaseDB, tags_releases
from database.TagDB import TagDB
from main import get_db

config = Config()
db: Session = next(get_db())


def create_base_tag(name: str, fg: str, bg: str):
    t = TagDB.get_or_create(name, db)
    t.foreground_color = fg
    t.background_color = bg
    db.add(t)
    db.commit()


def create_base_tags():
    create_base_tag("Black", "#FFFFFF", "#000000")
    create_base_tag("Death", "#000000", "#E06666")
    create_base_tag("Heavy", "#000000", "#508BBF")
    create_base_tag("Power", "#000000", "#9EC2D4")
    create_base_tag("Doom", "#000000", "#674EA7")
    create_base_tag("Sludge", "#000000", "#A64D79")
    create_base_tag("Thrash", "#000000", "#E2D469")
    create_base_tag("Grindcore", "#000000", "#7AB360")
    create_base_tag("Speed", "#000000", "#88FBFF")
    create_base_tag("Prog", "#000000", "#BB9965")


def parse_tag(input: str) -> List[str]:
    special_genres = {
        "Neoclassical Black": ["Black", "Neoclassical"],
        "Brutal Death": ["Death", "Brutal"],
        "Progressive Brutal Death": ["Death", "Brutal", "Prog"],
        "Deathgrind": ["Death", "Grindcore"],
        "Slam": ["Death", "Slam"],
        "Atmospheric Black": ["Black", "Atmospheric"],
        "Atmospheric Death": ["Death", "Atmospheric"],
        "Atmo Black": ["Black", "Atmospheric"],
        "Atmospheric / Melodic Black": ["Black", "Atmospheric", "Melodic"],
        "Melodic Black": ["Black", "Melodic"],
        "Melodic Death": ["Death", "Melodic"],
        "Melodic Death/Grind": ["Death", "Grindcore", "Melodic"],
        "Raw Black": ["Black", "Raw"],
        "Raw black": ["Black", "Raw"],
        "Raw / Atmospheric Black": ["Black", "Raw", "Atmospheric"],
        "Experimental Death": ["Death", "Experimental"],
        "Noise / Experimental Death": ["Death", "Experimental", "Noise"],
        "Experimental Deathgrind": ["Death", "Grindcore", "Experimental"],
        "Epic Doom": ["Doom", "Epic"],
        "Funeral Doom": ["Doom", "Funeral"],
        "Technical Death": ["Death", "Technical"],
        "Death/Goregrind": ["Death", "Grindcore", "Gore"],
        "War": ["Black", "Death", "War"],
        "Blackened Death": ["Black", "Death"],
    }

    aliases = {
        "Grind": "Grindcore",
        "PV": "Powerviolence",
        "Death Metal": "Death",
        "Heavy Metal": "Heavy",
        "Black Metal": "Black",
    }
    input = input.strip()
    if input in special_genres:
        return special_genres[input]
    input = input.replace(",", "/")
    parts = input.split("/")
    parts = [p.strip() for p in parts]
    parts = [aliases.get(p, p) for p in parts]
    return parts


db.execute(tags_releases.delete())
db.execute(delete(LinkDB))
db.execute(delete(ReleaseDB))
db.execute(delete(TagDB))
create_base_tags()
db.commit()


csvreader = reader(open("import/Shreddit Release Schedule - Sheet1.csv"))

for idx, line in enumerate(csvreader):
    if idx < 7:
        continue
    artist, title, rtype, genre, label, rdate, description, link = line
    release = ReleaseDB()
    release.artist = artist.strip()
    release.title = title.strip()
    release.release_type = rtype.strip()
    release.release_date = parse(rdate)
    release.description = description.strip()
    release.label = LabelDB.get_or_create(label.strip(), db)

    link_entry = LinkDB()
    link_entry.url = link
    link_entry.release = release
    tags = parse_tag(genre)
    for t in tags:
        tag = TagDB.get_or_create(t, db)
        release.tags.append(tag)
    db.add(release)
    db.add(link_entry)
    db.commit()
    find_cover_art(release, db, config.COVERS_DIR)
