import 'reflect-metadata';
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
//import 'buefy/dist/buefy.css'
import '@/styles/bulma.scss'
import dayjs from 'dayjs'
import localizedFormat from 'dayjs/plugin/localizedFormat' // import plugin
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore' // import plugin
import weekOfYear from 'dayjs/plugin/weekOfYear' // import plugin
import weekYear from 'dayjs/plugin/weekYear' // import plugin
import utc from 'dayjs/plugin/utc' // import plugin

import "./validate";

dayjs.extend(localizedFormat)
dayjs.extend(isSameOrBefore)
dayjs.extend(weekYear)
dayjs.extend(weekOfYear)
dayjs.extend(utc)

Vue.use(Buefy, {
      defaultIconPack: 'fas'
    }
)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
