import Release from "@/model/Release";
import releaseAPI from "@/api/releaseAPI";

interface ReleaseStoreState {
    releases: Release[]
}

interface ReleaseStoreInterface {
    state: ReleaseStoreState
    loadReleases(): void
}

const ReleaseStore: ReleaseStoreInterface = {
    state: {
        releases: []
    },
    loadReleases(): void {
        releaseAPI.getReleases().then(r => {
            this.state.releases = r.releases
        })
    }
}

export default ReleaseStore