import AuthData from "@/model/AuthData";
import {plainToInstance} from "class-transformer";

interface AuthStoreState {
    authData: AuthData | null
}

interface AuthStoreInterface {
    state: AuthStoreState
    setAuthData(data: AuthData): void
    logout(): void
    save(): void
    load(): void
}

const AuthStore: AuthStoreInterface = {
    state: {
        authData: null
    },
    setAuthData(data: AuthData) {
        this.state.authData = data
        this.save()
    },
    logout() {
        this.state.authData = null
        this.save()
    },
    save() {
        if (this.state.authData) {
            localStorage.setItem("authData", JSON.stringify(this.state.authData))
        } else {
            localStorage.setItem("authData", "")
        }
    },
    load() {
        const json = localStorage.getItem("authData")
        if (json) {
            const data = JSON.parse(json)
            this.state.authData = plainToInstance(AuthData, data)
        }
    }
}

export default AuthStore