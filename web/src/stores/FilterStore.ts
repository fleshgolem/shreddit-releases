interface FilterStoreState {
    upcoming: boolean
    includedTags: string[]
    excludedTags: string[]
    releaseType: string
    label: string
    searchTerm: string
}

interface FilterStoreInterface {
    state: FilterStoreState
    toggleUpcoming(value: boolean): void
    toggleIncludedTag(value: string): void
    toggleExcludedTag(value: string): void
    selectReleaseType(value: string): void
    selectLabel(value: string): void
    enterSearchTerm(value: string): void
    resetIncludedTags(): void
    resetExcludedTags(): void
}

const FilterStore: FilterStoreInterface = {
    state: {
        upcoming: false,
        includedTags: [],
        excludedTags: [],
        releaseType: "",
        label: "",
        searchTerm: ""
    },
    toggleUpcoming(value: boolean) {
        this.state.upcoming = value
    },

    toggleIncludedTag(value: string) {
        let set = this.state.includedTags
        if (set.includes(value)) {
            set = set.filter(t => { return value != t })
        } else {
            set.push(value)
        }
        this.state.includedTags = set
    },

    toggleExcludedTag(value: string) {
        let set = this.state.excludedTags
        if (set.includes(value)) {
            set = set.filter(t => { return value != t })
        } else {
            set.push(value)
        }
        this.state.excludedTags = set
    },

    resetIncludedTags() {
        this.state.includedTags = []
    },

    resetExcludedTags() {
        this.state.excludedTags = []
    },

    selectReleaseType(value: string) {
      this.state.releaseType = value
    },

    selectLabel(value: string) {
        this.state.label = value
    },

    enterSearchTerm(value: string) {
        this.state.searchTerm = value
    }
}

export default FilterStore