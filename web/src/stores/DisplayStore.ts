interface DisplayStoreState {
    showTable: boolean,
    isLoading: boolean
}

interface DisplayStoreInterface {
    state: DisplayStoreState
    toggleShowTable(value: boolean): void
    toggleIsLoading(value: boolean): void
}

const DisplayStore: DisplayStoreInterface = {
    state: {
        showTable: false,
        isLoading: false
    },
    toggleShowTable(value: boolean) {
        this.state.showTable = value
    },

    toggleIsLoading(value: boolean) {
        this.state.isLoading = value
    }
}

export default DisplayStore