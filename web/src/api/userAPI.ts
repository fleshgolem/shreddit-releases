import baseRequest, {baseRequestVoid} from "@/api/base";
import LoginRequest from "@/model/LoginRequest";
import AuthData from "@/model/AuthData";
import {instanceToPlain} from "class-transformer";
import ProfileRequest from "@/model/ProfileRequest";
import UserList from "@/model/UserList";
import UserRequest from "@/model/UserRequest";
import User from "@/model/User";

const userAPI = {
    login(request: LoginRequest): Promise<AuthData> {
        return baseRequest('/api/login',
            'post',
            false,
            instanceToPlain(request),
            AuthData)
    },
    logout(): Promise<void> {
        return baseRequestVoid('/api/logout',
            'post',
            true,
            null)
    },
    checkAuth(): Promise<void>  {
        return baseRequestVoid('/api/auth',
            'get',
            true,
            null)
    },
    updateUserData(request: ProfileRequest): Promise<AuthData> {
        return baseRequest('/api/user',
            'put',
            true,
            instanceToPlain(request),
            AuthData)
    },
    listUsers(): Promise<UserList> {
        return baseRequest('/api/users',
            'get',
            true,
            null,
            UserList)
    },
    createUser(request: UserRequest): Promise<User> {
        return baseRequest('/api/users',
            'post',
            true,
            request,
            User)
    },
    editUser(id: string, request: UserRequest): Promise<User> {
        return baseRequest(`/api/users/${id}`,
            'put',
            true,
            request,
            User)
    },
    deleteUser(id: string): Promise<void> {
        return baseRequestVoid(`/api/users/${id}`,
            'delete',
            true,
            null)
    }
}

export default userAPI