import axios from "axios";
import {Method} from "axios";
import AuthStore from "@/stores/AuthStore";
import {plainToInstance} from "class-transformer";
import {ClassConstructor} from "class-transformer/types/interfaces";
import DisplayStore from "@/stores/DisplayStore";

declare global {
    interface Window { _env_: Record<string, string>; }
}

export const BASE_URL = window._env_.API_URL

axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    DisplayStore.toggleIsLoading(false);
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    DisplayStore.toggleIsLoading(false);

    if (error.response.status == 401) {
        AuthStore.logout()
    }
    return Promise.reject(error);
  });

function baseRequest<T>(path: string,
                        method: Method,
                        authorized = false,
                        data: Record<string, any> | null,
                        cls: ClassConstructor<T>): Promise<T> {
    const headers: Record<string, any> = {
    }
    if (authorized && AuthStore.state.authData?.token) {
        headers["Authorization"] = `Bearer ${AuthStore.state.authData?.token}`
    }
    DisplayStore.toggleIsLoading(true)
    return axios.request({
        method: method,
        url: path,
        baseURL: BASE_URL,
        headers: headers,
        data: data
    }).then(r => {
        return plainToInstance(cls, r.data)
    })
}

export function baseRequestVoid(path: string,
                        method: Method,
                        authorized = false,
                        data: Record<string, any> | null): Promise<void> {
    const headers: Record<string, any> = {
    }
    if (authorized && AuthStore.state.authData?.token) {
        headers["Authorization"] = `Bearer ${AuthStore.state.authData?.token}`
    }
    DisplayStore.toggleIsLoading(true)
    return axios.request({
        method: method,
        url: path,
        baseURL: BASE_URL,
        headers: headers,
        data: data
    })
}

export default baseRequest;