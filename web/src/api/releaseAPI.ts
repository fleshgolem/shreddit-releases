import Release, { ReleaseResponse } from "@/model/Release"
import baseRequest, {baseRequestVoid} from "@/api/base";
import ReleaseRequest from "@/model/ReleaseRequest";

export default {
    getReleases (): Promise<ReleaseResponse> {
        return baseRequest("/api/releases",
            "get",
            false,
            null,
            ReleaseResponse)
    },
    addRelease (request: ReleaseRequest): Promise<Release> {
        return baseRequest("/api/releases",
            "post",
            true,
            request,
            Release
            )
    },
    editRelease (id: number, request: ReleaseRequest): Promise<Release> {
        return baseRequest(`/api/releases/${id}`,
            "put",
            true,
            request,
            Release
            )
    },
    deleteRelease(id: number): Promise<void> {
        return baseRequestVoid(`/api/releases/${id}`,
            'delete',
            true,
            null)
    }

}