import Release from "@/model/Release";
import Tag from "@/model/Tag";
import _ from "lodash";

function availableReleaseTypes(releases: Release[]): string[] {
    const releaseTypes = releases.map(r => { return r.release_type })
    return _.uniq(releaseTypes)
}

export default availableReleaseTypes