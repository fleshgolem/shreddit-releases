import Release from "@/model/Release";
import Tag from "@/model/Tag";
import _ from "lodash";

function availableLabels(releases: Release[]): string[] {
    const labels = releases.map(r => { return r.label.name })
    return _.uniq(labels).sort()
}

export default availableLabels