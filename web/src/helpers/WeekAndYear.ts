import dayjs from "dayjs";

class WeekAndYear {
    year: number
    week: number

    constructor(year: number, week: number) {
        this.year = year
        this.week = week
    }

    get string(): string {
        return `${this.year}-${this.week}`
    }

    static fromString(value: string): WeekAndYear {
        const arr = value.split("-")
        return new WeekAndYear(parseInt(arr[0]), parseInt(arr[1]))
    }

    get isUpcoming(): boolean {
        const now = dayjs()
        if (now.weekYear() === this.year) {
            return this.week > now.week()
        }
        return this.year > now.year()
    }

    get displayString(): string {
        const now = dayjs()
        if (now.weekYear() === this.year && now.week() === this.week) {
            return "This Week"
        }
        if (now.weekYear() === this.year && now.week() + 1 === this.week) {
            return "Next Week"
        }
        return `${this.year} - Week ${this.week}`
    }
}

export default WeekAndYear