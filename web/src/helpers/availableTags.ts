import Release from "@/model/Release";
import FilterStore from "@/stores/FilterStore";
import Tag from "@/model/Tag";
import _ from 'lodash'
import filterReleases from "@/helpers/filterReleases";

function availableTags(releases: Release[], forcedAvailable: string[]): Tag[] {
    const forcedTags = _.flatMap(releases, (r: Release) => {
        return r.tags
    }).filter((t: Tag) => {
        return forcedAvailable.includes(t.name)
    })

    const allTags = _.flatMap(filterReleases(releases), (r: Release) => {
        return r.tags
    }).filter((t: Tag) => {
        return !forcedAvailable.includes(t.name)
    })

    const uniqForced = _.uniqBy(forcedTags, (t: Tag) => {
        return t.name
    })

    const uniqTags = _.uniqBy(allTags, (t: Tag) => {
        return t.name
    })

    const result = uniqForced.sort((a: Tag, b: Tag) => {
        return a.order < b.order ? -1 : 1
    })

    const sorted = uniqTags.sort((a: Tag, b: Tag) => {
        return a.order < b.order ? -1 : 1
    })

    result.push(...sorted)
    return result

}

export default availableTags