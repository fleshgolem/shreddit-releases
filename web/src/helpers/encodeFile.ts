
interface EncodedFile {
    data: string
    extension: string
}

function encodeFile(file: File | null): Promise<EncodedFile | null> {
    if (file) {
        const extension = file.name.split(".").pop() ?? ""
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                resolve({
                    data: (reader.result as string).split(',').pop() ?? "",
                    extension: extension
                });
            }

            reader.onerror = error => reject(error);
        });
    } else {
        return new Promise((resolve) => {
            resolve(null)
        })
    }
}

export default encodeFile