import Release from "@/model/Release";
import FilterStore from "@/stores/FilterStore";
import dayjs from "dayjs";
import Tag from "@/model/Tag";

function includedTagsMatch(releaseTags: Tag[], includedTags: string[]): boolean {
    const releaseTagStrings = releaseTags.map(t => { return t.name })
    for (const t of includedTags) {
        if (!releaseTagStrings.includes(t)) {
            return false
        }
    }
    return true
}

function excludedTagsMatch(releaseTags: Tag[], excludedTags: string[]): boolean {
    const releaseTagStrings = releaseTags.map(t => { return t.name })
    for (const t of excludedTags) {
        if (releaseTagStrings.includes(t)) {
            return false
        }
    }
    return true
}

export function baseFilterReleases(releases: Release[]): Release[] {
    let result = releases;
    if (FilterStore.state.includedTags.length > 0) {
        result = result.filter(r => {
            return includedTagsMatch(r.tags, FilterStore.state.includedTags)
        })
    }
    if (FilterStore.state.excludedTags.length > 0) {
        result = result.filter(r => {
            return excludedTagsMatch(r.tags, FilterStore.state.excludedTags)
        })
    }

    if (FilterStore.state.releaseType) {
        result = result.filter(r => {
            return r.release_type === FilterStore.state.releaseType
        })
    }

    if (FilterStore.state.label) {
        result = result.filter(r => {
            return r.label.name === FilterStore.state.label
        })
    }

    if (FilterStore.state.searchTerm) {
        const term = FilterStore.state.searchTerm
        result = result.filter(r => {
            return r.artist.toLowerCase().includes(term.toLowerCase())
                || r.title.toLowerCase().includes(term.toLowerCase())
                || r.description.toLowerCase().includes(term.toLowerCase())
        })
    }

    return result
}

function filterReleases(releases: Release[]): Release[] {
    let result = baseFilterReleases(releases)
    result = result.filter(r => {
        return r.release_date.isSameOrBefore(dayjs(), 'day') !== FilterStore.state.upcoming
    })
    return result;
}

export default filterReleases