import Release from "@/model/Release";
import FilterStore from "@/stores/FilterStore";
import dayjs from "dayjs";
import filterReleases, {baseFilterReleases} from "@/helpers/filterReleases";
import WeekAndYear from "@/helpers/WeekAndYear";
import _ from "lodash";



export interface ReleaseGrouping {
    weeks: WeekAndYear[]
    releases: { [key:string] : Release[] }
}

export function sortedReleases(releases: Release[], ascending=false): Release[] {
    const order = ascending ? 1 : -1
    return releases.sort((a, b) => {
        if (a.release_date.isSame(b.release_date, "day")) {
            return a.key > b.key ? 1 : -1
        }
      return a.release_date > b.release_date ? order : (-1 * order)
    })
  }

function groupReleases(releases: Release[]): ReleaseGrouping {
    const availReleases = sortedReleases(baseFilterReleases(releases), FilterStore.state.upcoming)
    let weeks = []
    const groups: { [key:string] : Release[] } = {}
    for (const r of availReleases) {
        const day = r.release_date
        const week = new WeekAndYear(day.weekYear(), day.week())
        if (!groups[week.string]) {
            weeks.push(week)
            groups[week.string] = []
        }
        groups[week.string].push(r)
    }
    weeks = weeks.filter(w => {
        return w.isUpcoming == FilterStore.state.upcoming
    })
    return {
        weeks: weeks,
        releases: groups
    }
}

export default groupReleases