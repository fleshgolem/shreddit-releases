import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Login from "@/views/Login.vue";
import Profile from "@/views/Profile.vue";
import AddRelease from "@/views/AddRelease.vue";
import ManageUsers from "@/views/ManageUsers.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/addRelease',
    name: 'AddRelease',
    component: AddRelease
  },
  {
    path: '/editRelease/:id',
    name: 'EditRelease',
    component: AddRelease
  },
  {
    path: '/users',
    name: 'Users',
    component: ManageUsers
  }
]

const router = new VueRouter({
  routes
})

export default router
