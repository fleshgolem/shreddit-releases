import { Expose, Type, Transform } from 'class-transformer';
import Tag from "@/model/Tag";
import Label from "@/model/Label";
import Link from "@/model/Link";
import { BASE_URL } from "@/api/base";
import dayjs, {Dayjs} from 'dayjs';

export class Release {
    @Expose() id: number
    @Expose() artist: string
    @Expose() title: string
    @Expose() release_type: string
    @Expose() description: string

    @Transform(({ value }) => dayjs(value))
    @Expose() release_date: Dayjs

    @Transform(({ value }) => dayjs.utc(value))
    @Expose() date_added: Date

    @Type(() => Tag)
    @Expose() tags: Tag[];

    @Type(() => Link)
    @Expose() links: Link[];

    @Expose() label: Label;

    @Expose() cover_url: string | undefined;
    @Expose() cover_thumbnail_url: string | undefined;

    get full_cover_url(): string | undefined {
        if (!this.cover_url) {
            return undefined
        }
        return `${BASE_URL}${this.cover_url}`
    }

    get full_thumbnail_cover_url(): string | undefined {
        if (!this.cover_thumbnail_url) {
            return undefined
        }
        return `${BASE_URL}${this.cover_thumbnail_url}`
    }

    get formatted_release_date(): string {
        return this.release_date.format('l')
    }

    get formatted_date_added(): string {
        return dayjs(this.date_added).utc().format('l')
    }

    get key(): string {
        return `${this.artist}-${this.title}`
    }
}

export class ReleaseResponse {
    @Type(() => Release)
    @Expose() releases: Release[];
}

export default Release;