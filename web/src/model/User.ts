import { Expose } from 'class-transformer';

export type Role = 'admin' | 'contributor'

class User {
    @Expose() id: string
    @Expose() username: string
    @Expose() role: Role
}

export default User