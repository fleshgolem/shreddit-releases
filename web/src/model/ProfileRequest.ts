import { Expose } from 'class-transformer';

class ProfileRequest {
    @Expose() username: string | null
    @Expose() old_password: string
    @Expose() new_password: string | null

    constructor(username: string | null, oldPassword: string, newPassword: string | null) {
        this.username = username
        this.old_password = oldPassword
        this.new_password = newPassword
    }
}

export default ProfileRequest