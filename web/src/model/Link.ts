import { Expose } from 'class-transformer';

class Link {
    @Expose() url: string
}

export default Link