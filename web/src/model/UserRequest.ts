import { Expose } from 'class-transformer';
import {Role} from "@/model/User";

class UserRequest {
    @Expose() username: string
    @Expose() password: string | null
    @Expose() role: Role

    constructor(username: string, password: string | null, role: Role) {
        this.username = username
        this.password = password
        this.role = role
    }
}

export default UserRequest