import { Expose } from 'class-transformer';

class Label {
    @Expose() name: string
}

export default Label