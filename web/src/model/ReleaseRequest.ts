import {Expose, Type} from "class-transformer";

class ReleaseRequest {
    @Expose() artist: string
    @Expose() title: string
    @Expose() release_type: string
    @Expose() description: string

    @Expose() release_date: string

    @Expose() tags: string[];
    @Expose() links: string[];

    @Expose() label: string;

    @Expose() cover_data: string | undefined | null;
    @Expose() cover_extension: string | undefined | null;

    constructor(artist: string,
                title: string,
                release_type: string,
                description: string,
                release_date: string,
                tags: string[],
                links: string[],
                label: string,
                cover_data: string | undefined | null,
                cover_extension: string | undefined | null) {
        this.artist = artist
        this.title = title
        this.release_type = release_type
        this.description = description
        this.release_date = release_date
        this.tags = tags
        this.links = links
        this.label = label
        this.cover_data = cover_data
        this.cover_extension = cover_extension
    }
}

export default ReleaseRequest