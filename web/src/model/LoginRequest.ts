import { Expose } from 'class-transformer';

class LoginRequest {
    @Expose() username: string
    @Expose() password: string

    constructor(username: string, password: string) {
        this.username = username
        this.password = password
    }
}

export default LoginRequest