import { Expose } from 'class-transformer';
import User from "@/model/User";

class AuthData {
    @Expose() user: User
    @Expose() token: string
}

export default AuthData