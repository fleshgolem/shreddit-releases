import {Expose, Type} from 'class-transformer';
import User from "@/model/User";
import Release from "@/model/Release";

class UserList {
    @Type(() => User)
    @Expose() users: User[];
}

export default UserList