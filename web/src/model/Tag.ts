import { Expose } from 'class-transformer';

class Tag {
    @Expose() name: string
    @Expose() order: number
    @Expose() foreground_color: string
    @Expose() background_color: string
}

export default Tag