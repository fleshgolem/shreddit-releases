# => Build container
FROM node:16-alpine as builder
WORKDIR /app
#ENV NODE_OPTIONS=--openssl-legacy-provider
COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .
RUN yarn build

# => Run container
FROM nginx:mainline-alpine

# Nginx config
RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx

# Static build
COPY --from=builder /app/dist /usr/share/nginx/html/

# Default port exposure
EXPOSE 80

# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY ./env.sh .

# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]