#!/bin/bash

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment
echo "window._env_ = {" >> ./env-config.js

echo "API_URL: \"$API_URL\"" >> ./env-config.js
echo "}" >> ./env-config.js